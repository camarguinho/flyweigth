package auto.estudo;

import java.util.List;

import org.jfugue.Player;


public class Instrumento {
	
	public void toca(List<Nota> musica){
		Player player = new Player();
		
		StringBuilder sb = new StringBuilder();
		
		for(Nota nota : musica){
			sb.append(nota.simbolo()).append(" ");
		}
		
		player.play(sb.toString());
	}
	
}
