package auto.estudo;

import java.util.ArrayList;
import java.util.List;

public class Programa {
	
	public static void main(String[] args) {
		
		String[] notasParam = {"do", "re", "mi", "fa", "fa", "fa", "do#", "re#"};
		
		List<Nota> musica = new ArrayList<Nota>();
		NotasMusicais notas = new NotasMusicais(notasParam);
		
		for(int i = 0; i < notasParam.length; i++){
			musica.add(notas.pega(notasParam[i]));
		}
		
		Instrumento instrumento = new Instrumento();
		instrumento.toca(musica);
	}
	
}
