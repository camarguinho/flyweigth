package auto.estudo;

import java.util.HashMap;
import java.util.Map;

public class NotasMusicais {

	private static Nota DoInstance;
	private static Nota ReInstance;
	private static Nota MiInstance;
	private static Nota FaInstance;
	private static Nota SolInstance;
	private static Nota LaInstance;
	private static Nota SiInstance;
	private static Nota DoSustenidoInstnce;
	private static Nota ReSustenidoInstance;

	private static Map<String, Nota> notas = new HashMap<String, Nota>();

	public NotasMusicais(String[] notasParam) {
		for(String obj : notasParam){
			if(obj.equals("do")){
				notas.put("do", DoInstance == null ? new Do() : DoInstance);
			}
			if(obj.equals("re")){
				notas.put("re", ReInstance == null ?  new Re() : ReInstance);
			}
			if(obj.equals("mi")){
				notas.put("mi", MiInstance == null ? new Mi() : MiInstance);
			}
			if(obj.equals("fa")){
				notas.put("fa", FaInstance == null ? new Fa() : FaInstance);
			}
			if(obj.equals("sol")){
				notas.put("sol", SolInstance == null ? new Sol() : SolInstance);
			}
			if(obj.equals("la")){
				notas.put("la", LaInstance == null ? new La() : LaInstance);
			}
			if(obj.equals("si")){
				notas.put("si", SiInstance == null ? new Si() : SiInstance);
			}
			if(obj.equals("do#")){
				notas.put("do#", DoSustenidoInstnce == null ? new DoSustenido() : DoSustenidoInstnce);
			}
			if(obj.equals("re#")){
				notas.put("re#", ReSustenidoInstance == null ? new ReSustenido() : ReSustenidoInstance);
			}
		}
		System.out.println(notas);
	}
	
	
	public Nota pega(String chave){
		return notas.get(chave);
	}

}
